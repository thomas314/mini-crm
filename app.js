// Premiere personne 

$.getJSON("data/crm.json", function (data){
    console.log(data);

    // Nos données
// En-tête
    var nom = data.customers[0].firts_name + " " + data.customers[0].last_name;
    var img = data.customers[0].picture; //url
    var birthday = data.customers[0].birthday;
    var email = data.customers[0].email;
    var phone = data.customers[0].phone;
    var company = data.customers[0].company;
    var title = data.customers[0].title;
    var description = data.customers[0].desciption;
// Notes   
    var Titre1 = "Notes";
    var subject = data.customers[0].notes[0].subject;
    var note = data.customers[0].notes[0].note;
    var related = data.customers[0].notes[0].related_to;
// Tasks
    var Titre2 = "Tasks";
    var task = data.customers[0].tasks[0].task;
    var category = data.customers[0].tasks[0].category;
    var date = data.customers[0].tasks[0].date;
    var owner = data.customers[0].tasks[0].owner;
    var priority = data.customers[0].tasks[0].priority;
    var status = data.customers[0].tasks[0].status;
    var related2 = data.customers[0].tasks[0].related_to;
    var relateddeal = data.customers[0].tasks[0].related_deal;
//Tags 
    var Titre3 = "Tags";
    var tags = data.customers[0].tags;
    // Jquery
    // Div générale container
    $("<div id='content'></div>").appendTo("#app");
    // Div entete + image 

    $("<div id='entete'></div>").appendTo("#content");
    $("<h1 id='nom'></h1>").appendTo("#entete");
    $("#nom").append(nom);
    $("<img></img>").appendTo("#entete");
    $("img").attr("src", img);
    $("<p id='birth'></p>").appendTo("#entete")
    $("#birth").append(birthday);
    $("<p id='email'></p>").appendTo("#entete")
    $("#email").append(email);
    $("<p id='phone'></p>").appendTo("#entete")
    $("#phone").append(phone);
    $("<p id='company'></p>").appendTo("#entete")
    $("#company").append(company);
    $("<p id='title'></p>").appendTo("#entete")
    $("#title").append(title);
    $("<p id='description'></p>").appendTo("#entete")
    $("#description").append(description);

// Div parent de notes et tasks :

    $("<div id='papa'></div>").appendTo("#content");
    
    // Div Notes

    $("<div id='notes'></div>").appendTo("#papa");
    $("<h1 id='Titre1'></h1>").appendTo("#notes");
    $("#Titre1").append(Titre1);
    $("<p id='subject'></p>").appendTo("#notes");
    $("#subject").append(subject);
    $("<p id='note'></p>").appendTo("#notes");
    $("#note").append(note);
    $("<p id='relatedto'></p>").appendTo("#notes");
    $("#relatedto").append(related);
    
    // Div tasks

    $("<div id='tasks'></div>").appendTo("#papa");
    $("<h1 id='Titre2'></h1>").appendTo("#tasks");
    $("#Titre2").append(Titre2);
    $("<p id='task'></p>").appendTo("#tasks");
    $("#task").append(task);
    $("<p id='category'></p>").appendTo("#tasks");
    $("#category").append(category);
    $("<p id='date'></p>").appendTo("#tasks");
    $("#date").append(date);
    $("<p id='owner'></p>").appendTo("#tasks");
    $("#owner").append(owner);
    $("<p id='priority'></p>").appendTo("#tasks");
    $("#priority").append(priority);
    $("<p id='status'></p>").appendTo("#tasks");
    $("#status").append(status);
    $("<p id='related2'></p>").appendTo("#tasks");
    $("#related2").append(related2);
    $("<p id='relatedD'></p>").appendTo("#tasks");
    $("#relatedD").append(relateddeal);
    // Div tags 
    $("<div id='tags'></div>").appendTo("#content");
    $("<h1 id='Titre3'></h1>").appendTo("#tags");
    $("#Titre3").append(Titre3);
    $("<p id='Tags'></p>").appendTo("#tags");
    $("#Tags").append(tags);
})
// $("<img src='" + img + "'></img>").appendTo("#app");



// Deuxieme personne 
$.getJSON("data/crm.json", function (data){
    console.log(data);

    //En tete 
    var nom2 = data.customers[1].firts_name + " " + data.customers[1].last_name;
    var img2 = data.customers[1].picture; //url
    var birthday2 = data.customers[1].birthday;
    var email2 = data.customers[1].email;
    var phone2 = data.customers[1].phone;
    var company2 = data.customers[1].company;
    var title2 = data.customers[1].title;
    var description2 = data.customers[1].desciption;

    // Notes   
    var Titre1bis = "Notes";
    var subject2 = data.customers[1].notes[0].subject;
    var note2 = data.customers[1].notes[0].note;
    var related2 = data.customers[1].notes[0].related_to;

    // Tasks
    var Titre2bis = "Tasks";
    var task2 = data.customers[1].tasks[0].task;
    var category2 = data.customers[1].tasks[0].category;
    var date2 = data.customers[1].tasks[0].date;
    var owner2 = data.customers[1].tasks[0].owner;
    var priority2 = data.customers[1].tasks[0].priority;
    var status2 = data.customers[1].tasks[0].status;
    var related2bis = data.customers[1].tasks[0].related_to;
    var relateddeal2 = data.customers[1].tasks[0].related_deal;

    //Tags 
    var Titre3bis = "Tags";
    var tags2 = data.customers[1].tags;
// Div générale container
$("<div id='content2'></div>").appendTo("#app");
// Div entete + image 

$("<div id='entete2'></div>").appendTo("#content2");
$("<h1 id='nom2'></h1>").appendTo("#entete2");
$("#nom2").append(nom2);
$("<img></img>").appendTo("#entete2");
$("img").attr("src", img2);
$("<p id='birth2'></p>").appendTo("#entete2")
$("#birth2").append(birthday2);
$("<p id='email2'></p>").appendTo("#entete2")
$("#email2").append(email2);
$("<p id='phone2'></p>").appendTo("#entete2")
$("#phone2").append(phone2);
$("<p id='company2'></p>").appendTo("#entete2")
$("#company2").append(company2);
$("<p id='title2'></p>").appendTo("#entete2")
$("#title2").append(title2);
$("<p id='description2'></p>").appendTo("#entete2")
$("#description2").append(description2);

// Div parent de notes et tasks :

$("<div id='papa2'></div>").appendTo("#content2");
    
// Div Notes

$("<div id='notes2'></div>").appendTo("#papa2");
$("<h1 id='Titre1bis'></h1>").appendTo("#notes2");
$("#Titre1bis").append(Titre1bis);
$("<p id='subject2'></p>").appendTo("#notes2");
$("#subject2").append(subject2);
$("<p id='note2'></p>").appendTo("#notes2");
$("#note2").append(note2);
$("<p id='relatedto2'></p>").appendTo("#notes2");
$("#relatedto2").append(related2);

// Div tasks

$("<div id='tasks2'></div>").appendTo("#papa2");
$("<h1 id='Titre2bis'></h1>").appendTo("#tasks2");
$("#Titre2bis").append(Titre2bis);
$("<p id='task2'></p>").appendTo("#tasks2");
$("#task2").append(task2);
$("<p id='category2'></p>").appendTo("#tasks2");
$("#category2").append(category2);
$("<p id='date2'></p>").appendTo("#tasks2");
$("#date2").append(date2);
$("<p id='owner2'></p>").appendTo("#tasks2");
$("#owner2").append(owner2);
$("<p id='priority2'></p>").appendTo("#tasks2");
$("#priority2").append(priority2);
$("<p id='status2'></p>").appendTo("#tasks2");
$("#status2").append(status2);
$("<p id='related2'></p>").appendTo("#tasks2");
$("#related2").append(related2);
$("<p id='relatedD2'></p>").appendTo("#tasks2");
$("#relatedD2").append(relateddeal2);
// Div tags 
$("<div id='tags2'></div>").appendTo("#content2");
$("<h1 id='Titre3bis'></h1>").appendTo("#tags2");
$("#Titre3bis").append(Titre3bis);
$("<p id='Tags2'></p>").appendTo("#tags2");
$("#Tags2").append(tags2);
})

// Troisieme personne 
$.getJSON("data/crm.json", function (data){
    console.log(data);

    //En tete 
    var nom3 = data.customers[2].firts_name + " " + data.customers[2].last_name;
    var img3 = data.customers[2].picture; //url
    var birthday3 = data.customers[2].birthday;
    var email3 = data.customers[2].email;
    var phone3 = data.customers[2].phone;
    var company3 = data.customers[2].company;
    var title3 = data.customers[2].title;
    var description3 = data.customers[2].desciption;

    //Tags 
    var Titre3ter = "Tags";
    var tags3 = data.customers[2].tags;
// Div générale container
$("<div id='content3'></div>").appendTo("#app");
// Div entete + image 

$("<div id='entete3'></div>").appendTo("#content3");
$("<h1 id='nom3'></h1>").appendTo("#entete3");
$("#nom3").append(nom3);
$("<img></img>").appendTo("#entete3");
$("img").attr("src", img3);
$("<p id='birth3'></p>").appendTo("#entete3")
$("#birth3").append(birthday3);
$("<p id='email3'></p>").appendTo("#entete3")
$("#email3").append(email3);
$("<p id='phone3'></p>").appendTo("#entete3")
$("#phone3").append(phone3);
$("<p id='company3'></p>").appendTo("#entete3")
$("#company3").append(company3);
$("<p id='title3'></p>").appendTo("#entete3")
$("#title3").append(title3);
$("<p id='description3'></p>").appendTo("#entete3")
$("#description3").append(description3);


// Div tags 
$("<div id='tags3'></div>").appendTo("#content3");
$("<h1 id='Titre3ter'></h1>").appendTo("#tags3");
$("#Titre3ter").append(Titre3ter);
$("<p id='Tags3'></p>").appendTo("#tags3");
$("#Tags3").append(tags3);
})
